import smbus
import time

WRITE_DATA = 0xD2
READ_DATA = 0xD3
REG1 = 0x20
REG2 = 0x21
REG3 = 0x22
REG4 = 0x23
REG5 = 0x24
STAT = 0x27
OUT_X_INC = 0xA8

REG3_DEF = 0x08
REG4_DEF = 0x08
REG1_DEF = 0x1F

def get_i2c_bus(channel = 1, address = 0x69):
	bus = smbus.SMBus(channel)
	# magic driver stuff
	bus.write_byte_data(address, REG3, REG3_DEF)
	bus.write_byte_data(address, REG4, REG4_DEF)
	bus.write_byte_data(address, REG1, REG1_DEF)
	return bus

def convert_raw(high,low):
	# deal with funky bit format for negatives #
	if high>127:
		return ((high-256)*256.0+(low-256.0))/114.0
	else: 
		return ((high)*256.0+(low))/114.0

if __name__=="__main__":
	channel = 0x01 # which i2c channel we use (other one is 0, 2 does not exist)
	address = 0x69 # i2c name of the gyro. Find it with i2cdetect -y 1
	bus = get_i2c_bus(channel, address) 
	for i in range(0,2000):
                # reads some block data and converts it
		reg = bus.read_i2c_block_data(address, OUT_X_INC, 6)
		x=convert_raw(reg[1],reg[0])
		y=convert_raw(reg[3],reg[2])
		z=convert_raw(reg[5],reg[4])
		print x,y,z, reg
		time.sleep(0.1)


