import RPi.GPIO as GPIO
import time
import csv
import numpy as np

def init():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(18,GPIO.IN)
    GPIO.setup(21,GPIO.OUT)
    pass

def light():
    for i in range(2):
        GPIO.output(21,GPIO.LOW)
        time.sleep(0.1)
        GPIO.output(21,GPIO.HIGH)
        time.sleep(0.1)
        

def detect():
    gt0 =time.strftime('%H%M%S',time.gmtime())
    gtt = []
    temp=0
    duration=[]
    for ii in range(57):
      if GPIO.input(18) == True:
          gt = time.strftime('%H%M%S',time.gmtime())
          a = int(gt[-2:])-int(gt0[-2:])
        #print a
        #print gt[-2:-1]
        #print gt
          if a < 0:
              a += 60
          if a >= 4:
              gtt.append(gt)
              light()
              gt0 = gt
              temp+=1
              duration.append(a)
          else:
              #print "still moving"
              light()
              gt0 = gt
           
            
      else:
          #print "nobody"
          pass
      time.sleep(0.8)
    gtt.append(str(temp))
    norm_dur=[0,0,0]
    if len(duration)>=1:
        norm_dur=[np.mean(duration),np.std(duration),len(duration)]
    
    #result = ''.join(gtt)
    return gtt,duration,norm_dur

#time.sleep(2)
init()
gtt,duration,norm_dur=detect()

    
print norm_dur
GPIO.cleanup()
