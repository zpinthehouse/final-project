#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import sys

def init():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(18,GPIO.IN)
    GPIO.setup(21,GPIO.OUT)
    pass

def light():
    for i in range(5):
        GPIO.output(21,GPIO.LOW)
        time.sleep(0.1)
        GPIO.output(21,GPIO.HIGH)
        time.sleep(0.1)

def detect():
    if GPIO.input(18) == True:
        a=1
    else:
        a=0
    return a

init()
print detect()
GPIO.cleanup()
